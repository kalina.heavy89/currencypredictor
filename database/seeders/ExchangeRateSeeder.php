<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\ExchangeRate;

class ExchangeRateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 5; $i++) {
            $date = date('Y-m-d');
            DB::table('exchange_rate')->insert([
                'id' => $i+1,
                'rate' => mt_rand(2650, 2950)/100,
                'rate_date' => date('Y-m-d', strtotime($date . $i . "days" . " -2 days"))
            ]);
        }
    }
}
