<?php


namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\Interfaces\BaseInterface;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class BaseRepository implements BaseInterface
{
    protected $model;

    public $sortBy = 'id';
    public $sortOrder = 'asc';
    public $count = 10;

    /**
    * Constructor.
    *
    * @var Model $model
    */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
    * Get all instances of model
    *
    * @return Collection
    */
    public function all(): Collection
    {
        return $this->model
            ->orderBy($this->sortBy, $this->sortOrder)
            ->get();
    }

    /**
    * Create a new record in the database
    *
    * @param array $data
    * @return model
    */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
    * Update record in the database and get data back
    *
    * @param int $id
    * @param array $data
    * @return boolean
    */
    public function update(int $id, array $data)
    {
        //$query = $this->model->where('id', $id);
        return $query = $this->model->updateOrCreate(['id' => $id], $data);
    }

    /**
    * Remove record from the database
    *
    * @param int $id
    * @return boolean
    */
    public function destroy(int $id): bool
    {
        $this->model->destroy($id);
        return true;
    }

    /**
    * Show the record with the given id
    *
    * @param int $id
    * @return model
    */
    public function findById(int $id)
    {
        return $this->model->find($id);
    }

    /**
    * Show the record from field $fieldName with with $fieldValue
    *
    * @param string $fieldName
    * @param string $fieldValue
    * @return model
    */
    public function findByField(string $fieldName, string $fieldValue)
    {
        return $this->model->where($fieldName, $fieldValue)->first();
    }

    /**
    * Get the column by field name.
    *
    * @param string $fieldName
    * @return model
    */
    public function getByField(string $fieldName)
    {
        return $this->model->select($fieldName)->get();
    }

    /**
    * Show the first record from the database
    *
    * @return model
    */
    public function first()
    {
        return $this->model->first();
    }

    public function last()
    {
        return $this->model->latest()->first();
    }

    /**
    * Get the associated model
    *
    * @return Model
    */
    public function getModel(): Model
    {
        return $this->model;
    }

    /**
    * Set the associated model
    *
    * @param $model
    * @return $this
    */
    public function setModel(Model $model)
    {
        $this->model = $model;
        return true;
    }

}
