@extends('adminlte::page')

@section('content_header')
    <div class="starter-template text-center py-2 px-3">
        <h1>Personal Cabinet</h1>
    </div>
@stop

@section('content')
    <div class="container">
    <div class="row justify-content-center">
      @include('partials.alerts')
      <div class="container">
        @csrf                
  
              @if ($userData->access_cond == 'empty')
                    <p>You may use 
                        <a class="btn btn-primary" href="{{ route('trial') }}"><b>trial</b></a>                        
                        period during 1 month.
                </p>
                <p>
                    If you use trial period you have exchange rate for 3 last 
                    days and 1 day for predicted exchange rate.
                </p>
                <p>
                  You can 
                        <a class="btn btn-success" href="{{ route('prices') }}"><b>buy</b></a>
                        the subscription.
                    </p>
                <p>
                    If you use subscription you have exchange rate for 3 last 
                    days and 3 days for predicted exchange rate.
                </p>
                @elseif ($userData->access_cond == 'expired')
                    <p>
                        Your access to exchange rate is expired. <br>
                        You can 
                        <a class="btn btn-success" href="{{ route('prices') }}"><b>buy</b></a>
                        the subscription.
                    </p>
                @elseif ($userData->access_cond == 'trial')
                    <p>
                        You are using a trial period. <br>
                        You can 
                        <a class="btn btn-success" href="{{ route('prices') }}"><b>buy</b></a>
                        the subscription.
                    </p>
                @elseif ($userData->access_cond == 'subscripted')
                    <p>
                        You use a subscription. <br>
                        You can 
                        <a class="btn btn-success" href="{{ route('prices') }}"><b>extend</b></a> 
                        the subscription.
                    </p>
                @else
                @endif

                @if (($userData->access_cond == 'trial') || ($userData->access_cond == 'subscripted'))
                    <p>
                      Your access to predicted exchange rate is available to <b> 
                      {{ App\Helpers\Helper::changeDateFormat(Auth::user()->end_acc_period, "jS \of F Y") }} year </b>.
                    </p>
                @endif
            </div>
@stop
