<?php


namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\User;

class UserRepository extends BaseRepository
{
    /**
    * Constructor.
    *
    * @var User $model
    */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

}
