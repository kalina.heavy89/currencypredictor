<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\SubscriptionPriceRepository;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Laravel\Cashier\Cashier;

class SubscriptionService extends BaseService
{
    /**
     * Constructor.
     *
     * @var SubscriptionPriceRepository $repo
     * @var UserService $user
     */
    public function __construct(
        SubscriptionPriceRepository $repo,
        UserService $user
    ) {
        $this->repo = $repo;
        $this->user = $user;
    }

    /**
    * Get subscription prices from data base.
    *
    * @return Collection
    */
    public function getSubscriptionPrices()
    {
        return $this->repo->all();
    }

    /**
    * Set trial period.
    *
    * @param int $userId
    * @return boolean
    */
    public function setTrialPeriod($userId)
    {
        $begTrialPeriod = date("Y-m-d");
        $endTrialPeriod = date("Y-m-d", strtotime( '+1 month' ) );
        //Changing data in table users:
        $trialData = [
            'access_cond' => 'trial',
            'beg_acc_period' => $begTrialPeriod,
            'end_acc_period' => $endTrialPeriod
        ];
        return $this->user->update($userId, $trialData);
    }

    /**
    * Buy subscription using Stripe.
    *
    * @return array
    */
    public function buySubscription(Request $request)
    {
        $user = Auth::user();
        $user->newSubscription('cashier', $request->price)->create($request->paymentMethod);
        $startDate = '';
        if ($user->access_cond != 'subscripted') {
            $user->access_cond = 'subscripted';
            $startDate = date('Y-m-d');
        } else {
            $startDate = date('Y-m-d', strtotime($user->end_acc_period));;
        }
        $data_subscr = $this->repo->findByField('stripe_id', $request->price);
        $monthes = $data_subscr->months;
        $user->end_acc_period = date('Y-m-d', strtotime("+ " . $monthes . " months", strtotime($startDate)));
        $user->save();
        return true;
	}
}
