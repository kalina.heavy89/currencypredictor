@extends('adminlte::page')

@section('content_header')
    <div class="starter-template text-center py-2 px-3">
        <h1>Profile</h1>
    </div>
@stop

@section('content')
    <!--<div class="md-2"></div>-->
    <div class="md-8">
        <div class="card card-primary card-outline">
          <div class="card-body box-profile">
            @include('partials.alerts')

            <ul class="list-group list-group-unbordered mb-3">
              <li class="list-group-item">
                <b>Name</b> <a class="float-right">{{ $user->name }}</a>
              </li>
              <li class="list-group-item">
                <b>E-Mail Address</b> <a class="float-right">{{ $user->email }}</a>
              </li>
              <li class="list-group-item">
                <b>Access Condition</b> <a class="float-right">{{ $user->access_cond }}</a>
              </li>
              @if (($user->access_cond == 'trial') || ($user->access_cond == 'subscripted'))
                <li class="list-group-item">
                  <b>End Access Date</b> <a class="float-right">
                      {{ App\Helpers\Helper::changeDateFormat($user->end_acc_period, "jS \of F Y") }} year
                  </a>
                </li>
              @endif
            </ul>        

          </div>
          <!-- /.card-body -->
          <div class="card-footer box-profile" style="text-align: center;">
            <a href="{{ route('user.edit', ['user' => $user->id]) }}" class="btn btn-primary" style="margin-top: 20px;"><b>Edit Profile</b></a>
          </div>

        </div>
    </div>

@stop