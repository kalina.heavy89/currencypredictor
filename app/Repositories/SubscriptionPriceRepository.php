<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\SubscriptionPrice;

class SubscriptionPriceRepository extends BaseRepository
{
    /**
    * Constructor.
    *
    * @var SubscriptionPrice $model
    */
    public function __construct(SubscriptionPrice $model)
    {
        $this->model = $model;
    }

}
