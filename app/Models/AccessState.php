<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccessState extends Model
{
    use HasFactory;

    protected $table = 'access_states';

    public $incrementing = false;

    protected $fillable = [
        'id',
        'condition',
        'period'
    ];

}
