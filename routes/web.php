<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SubscriptionBuyController;
use App\Http\Controllers\PredictionController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing_page');
});

Auth::routes(['verify' => true]);

Route::middleware(['auth'])->group(function () {
    Route::prefix('home')->group(
        function () {
            Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
            Route::get('/trial', [SubscriptionBuyController::class, 'setTrialPeriod'])->name('trial');
            Route::resource('/user', UserController::class);
            Route::get('/prices', [SubscriptionBuyController::class, 'getSubscriptionPrices'])->name('prices');
            Route::middleware(['enabled.customer'])->get('/show', [PredictionController::class, 'showDataRate'])->name('show');
            Route::post('/subscribe', [SubscriptionBuyController::class, 'buySubscription'])->name('subscribe.post');
        }
    );
});