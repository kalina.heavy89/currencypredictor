<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\ExchangeRateRepository;
use App\Services\PredictedRateService;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;

class ExchangeRateService extends BaseService
{
    const ANALYZED_DAYS = 4;
    const PREDICTED_DAYS = 3;
    const PREVIOUS_COURSE_QUERY = 'https://api.privatbank.ua/p24api/exchange_rates?json&date=';
        //date format: d.m.y

    private $existingRate = [];
    private $predictedRate = [];
    private $dates = [];

    /**
     * Undocumented variable
     *
     * @var ExchangeRateRepository $repo
     */
    public $repo;

    /**
     * Constructor.
     *
     * @var ExchangeRateRepository $repo
     * @var PredictedRateService $predictedRateService
     */
    public function __construct(ExchangeRateRepository $repo,
        PredictedRateService $predictedRateService)
    {
        $this->repo = $repo;
        $this->predictedRateService = $predictedRateService;
    }

    /**
    * Get last data from array.
    *
    * @param array $array
    * @param int $numberLastData
    * @return array
    */
    public function getLastData(array $array, int $numberLastData): array
    {
        $lastData = [];
        $begLastDataNumber = count($array) - $numberLastData - 1;
        if ($begLastDataNumber < 0) {
            dd('Array size less than needed number of last Data');
        }
        for ($i = 0; $i < $numberLastData; $i++) {
            $lastData[] = $array[$begLastDataNumber + $i];
        }
        return $lastData;
    }

    /**
    * Get currency rate by PrivatBank API from current or previous day
    *
    * @param int $dateShiftPrevious
    * @return float
    */
    public function getRateAPI(int $dateShiftPrevious): float
    {
        $date = date("d.m.Y", strtotime(($dateShiftPrevious * (- 1)) . ' days'));
        $query = self::PREVIOUS_COURSE_QUERY . $date;

        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $query);
        $result = (string) $res->getBody();
        $allRates = json_decode($result, true);

        foreach ($allRates["exchangeRate"] as $numCurrency) {
            foreach ($numCurrency as $key => $val) {
                if (($key == "currency") && ($val != "USD")) {
                    break;
                }
                if ($key == "saleRate") {
                    return $val;
                }
            }
        }
        return 0;
    }

    /**
    * Check that data of existing rate is exist in ANALYZED_DAYS.
    * If not - function get missing data from PrivatBank API and write it to the
    * exchange_rate table.
    *
    * @return void
    */
    public function checkExistingRate()
    {
        $checkDay = date("Y-m-d", strtotime(((self::ANALYZED_DAYS - 1) * (- 1)) . ' days'));
        //Download data from table exchange_rate
        foreach ($this->repo->all() as $rate) {
            $existingRateAll[] = $rate->rate_date;
        }
        $dataSizeExistingRate = count($existingRateAll);
        if ($dataSizeExistingRate >= self::ANALYZED_DAYS) {
            $checkNumber = self::ANALYZED_DAYS;
        } else {
            $checkNumber = $dataSizeExistingRate;
        }
        $existingRateId = $dataSizeExistingRate;
        //Find checkDay in $existingRateAll
        $missingDays = self::ANALYZED_DAYS;
        $findCheckDay = false;
        for ($i = 0; $i < $checkNumber; $i++) {
            $missingDays--;
            if ($existingRateAll[$dataSizeExistingRate - 1 - $i] == $checkDay) {
                $findCheckDay = true;
                break;
            }
        }
        if ($findCheckDay == false) {
            $missingDays = self::ANALYZED_DAYS;
        }
        //add missing days to table exchange_rate
        for ($i = 0; $i < $missingDays ; $i++) {
            $rate = $this->getRateAPI(($missingDays - $i)); //get rate from previous days
            $date = date("Y-m-d", strtotime((($missingDays - $i - 1) * (- 1)) . ' days'));
            $this->repo->update(($existingRateId + $i+1),[
                'rate' => $rate,
                'rate_date' => $date,
            ]);
        }
    }

    public function analyzeExistingData($data, array $dates)
    {
        $missedDate = [];
        foreach ($data as $rate) {
            if (in_array($rate->rate_date, $dates)) {
                ;
            }
        }
    }

    /**
    * Get data rate from database and save it into array
    *
    * @return array
    */
    public function getDataRate()
    {
        $dataRate = [];
        $i = 0;
        foreach ($this->repo->all() as $rate) {
            if ($i == 0) {
                $i++;
                continue;
            }
            $dataRate['date'][] = $rate->rate_date;
            $dataRate['existingRate'][] = $rate->rate;
            $dataRate['predictedRate'][] = null;
            $i++;
        }
        $i--;
        $dataRate['predictedRate'][$i-1] = $dataRate['existingRate'][$i-1];
        if (Auth::user()->access_cond == 'subscripted') {
            foreach ($this->predictedRateService->all() as $rate) {
                $dataRate['date'][$i] = $rate->rate_date;
                $dataRate['existingRate'][$i] = null;
                $dataRate['predictedRate'][$i] = $rate->rate;
                $i++;
            }
        } else {
            $predictedData = $this->predictedRateService->first();
            $dataRate['date'][$i] = $rate->rate_date;
            $dataRate['existingRate'][$i] = null;
            $dataRate['predictedRate'][$i] = $predictedData->rate;
        }
        return $dataRate;
    }

    /**
    * Calculate predicted rate and write it to predicted_rate table
    *
    * @return bool
    */
    public function calculatePredictedRate () {
        $existingRate = [];
        $divRate = [];
        $predictedRate = [];
        $predictedDays = 3;
        foreach ($this->repo->all() as $rate) {
            $existingRate[] = $rate->rate;
        }
        for ($i = 1; $i < count($existingRate)-1; $i++) {
            $divRate[] = $existingRate[$i+1] - $existingRate[$i-1];
        }
        $growRate = array_sum($divRate) / count($divRate);
        $growCoefficient = 1 + ($growRate / end($existingRate));
        $predictedRate[0] = end($existingRate) * $growCoefficient;
        for ($i = 0; $i < ($predictedDays - 1); $i++) {
            $randGrow = mt_rand (5, 15) / 100;
            $randSign = mt_rand (0, 2);
            switch ($randSign) {
                case 1: $sign = -1;
                break;
                case 2: $sign = 1;
                break;
                default: $sign = 0;
            }
            $growCoefficient = $growCoefficient + ($sign * $randGrow);
            $predictedRate[$i + 1] = end($existingRate) * $growCoefficient;
        }
        //Writing predicted data in table predicted_rate
        for ($i = 0; $i < $predictedDays; $i++) {
            $this->predictedRateService->update(($i + 1), [
                'rate' => $predictedRate[$i],
                'rate_date' => date("Y-m-d", strtotime( ($i + 1) . ' days' ) )
            ]);
        }
        return true;
    }

    /**
    * Save exchange rate and predicted rate into the tables exchange_rate
    * and predicted rate respectively.
    *
    * @param string $rate
    * @return void
    */
    public function saveDataRate ($rate) {
        /*//Check that data in table is exist
        $exist = true;
        $countDays = 5;
        for($i = $countDays; $i > 1; $i--) {
            if (empty($this->repo->findById($i))) {
                $exist = false;
                break;
            }
        }
        $currentDate = date("Y-m-d");
        if($exist) { //If data in the table is exist*/

            $this->checkExistingRate();
            dd(1);

            //Checking the time when was last update of exchange_rate table
            $exchangeRate = $this->repo->findById($countDays);
            if ((Helper::dateAnalizing($exchangeRate->rate_date) < 0) &&
                (date("H", strtotime("+2 hours")) > 13)) {
                $days = $this->repo->all();
                for($i = 1; $i < $countDays; $i++) {//Shift data from next to previous row
                    $exchangeRate = $this->repo->update(($i), [
                        'rate' => $days[$i]->rate,
                        'rate_date' => $days[$i]->rate_date,
                    ]);
                }
                $exchangeRate = $this->repo->update($countDays, [
                    'rate' => $rate,
                    'rate_date' => $currentDate,
                ]);
                $this->calculatePredictedRate ();
            }
        /*} else {
            //Writing identical data in all rows of the table
            for ($i = 0; $i < $countDays; $i++) {
                $this->repo->update(($i + 1),[
                    'rate' => $rate,
                    'rate_date' => $currentDate,
                ]);
            }
        }*/
        return true;
    }


}
