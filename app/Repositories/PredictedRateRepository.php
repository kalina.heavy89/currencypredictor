<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\PredictedRate;

class PredictedRateRepository extends BaseRepository
{
    /**
    * Constructor.
    *
    * @var PredictedRate $model
    */
    public function __construct(PredictedRate $model)
    {
       $this->model = $model;
    }

}
