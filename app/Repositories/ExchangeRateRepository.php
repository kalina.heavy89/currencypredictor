<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\ExchangeRate;

class ExchangeRateRepository extends BaseRepository
{
    /**
    * Constructor.
    *
    * @var ExchangeRate $model
    */
    public function __construct(ExchangeRate $model)
    {
        $this->model = $model;
    }

    public function getDataInRange(array $dates)
    {
        return $this->model->whereIn('rate_date', $dates)->get();
    }

    public function getLastData (string $date)
    {
        return $this->model->where('rate_date', '>=', $date)->get();
    }

}
