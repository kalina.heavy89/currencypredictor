<?php

namespace App\Helpers;

class Helper
{
    /**
     * Analizing date
     *
     * @param string $date in format "Y-m-d-H-i"
     * @return 0, -1 or 1 if date is present, past or future respectively
     */
	public static function dateAnalizing($date)
	{
		$currentDate = date("Y-m-d-H-i", strtotime("+2 hours"));
		$currentDateArray = explode('-', $currentDate);
		$analizeDateArray = explode('-', $date);
		for ($i = 0; $i < count($analizeDateArray); $i++) {
			if ($analizeDateArray[$i] == $currentDateArray[$i]) {
				$result = 0;
			} elseif ($analizeDateArray[$i] > $currentDateArray[$i]) {
				$result = 1;
				break;
			} else {
				$result = -1;
				break;
			}
		}
		return $result;
	}

	/**
     * Change date format
     *
     * @param string $date
     * @param $dateFormat
     * @return string
     */
	public static function changeDateFormat($date, $dateFormat)
	{
		return date($dateFormat, strtotime($date));
	}
}
