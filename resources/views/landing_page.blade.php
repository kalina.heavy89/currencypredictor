@extends('layouts.template')

@section('header')

<nav class="navbar_landing">
  <div class="container-fluid">
    <div>
      <a class="navbar-brand" href="{{ route('login') }}">{{ __('Login') }}</a>
      <a class="navbar-brand" href="{{ route('register') }}">{{ __('Register') }}</a>
    </div>       
  </div>
</nav>
<div class="title_header py-2 px-3">
    <h1>Currency Predictor</h1>
</div>  
@endsection

@section('content')

<div class="container">  

    <p class="images">  
      <img src="{{URL::asset('/images/currency-predictor.png')}}" alt="Currency Predictor" style="height:350px;">
    </p>

    <p class="lead">This is site about currency predictor.<br> You may use trial period during 1 month or buy subscription.</p>


</div>
@endsection