<?php

namespace App\Http\Controllers;

use App\Services\ExchangeRateService;
use App\Services\PredictedRateService;

class PredictionController extends Controller
{
    private $existRate;

    /**
     * Create a new controller instance.
     *
     * @param ExchangeRateService $existRate
     * @param PredictedRateService $predictRate
     * @return void
     */
    public function __construct(ExchangeRateService $existRate,
        PredictedRateService $predictRate)
    {
        $this->existRate = $existRate;
        $this->predictRate = $predictRate;
    }

    /**
     * Show Data Rate.
     *
     * @return \Illuminate\View\View
     */
    public function showDataRate() {
        $dataRate = $this->existRate->getDataRate();
        $date = $dataRate['date'];
        $existingRate = $dataRate['existingRate'];
        $predictedRate = $dataRate['predictedRate'];

        return view('prediction_data')
            ->with('date',json_encode($date,JSON_NUMERIC_CHECK))
            ->with('existingRate',json_encode($existingRate,JSON_NUMERIC_CHECK))
            ->with('predictedRate',json_encode($predictedRate,JSON_NUMERIC_CHECK));
    }

}
