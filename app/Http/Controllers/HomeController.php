<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @param UserService $user
     * @return void
     */
    public function __construct(UserService $user)
    {
        $this->middleware(['auth','verified']);
        $this->user = $user;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $id = Auth::user()->id;
        $user = $this->user->checkUserAccCond($id);

		return view('home')->with(['userData' => $user]);
    }
}
