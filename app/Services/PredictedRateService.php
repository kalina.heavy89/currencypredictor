<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\PredictedRateRepository;

class PredictedRateService extends BaseService
{
    /**
    * Constructor.
    *
    * @var PredictedRateRepository $repo
    */
    public function __construct(PredictedRateRepository $repo)
    {
        $this->repo = $repo;
    }

}
