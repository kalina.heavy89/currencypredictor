<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @param UserService $user
     * @return void
     */
    public function __construct(UserService $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view(
            'user.index',
            ["user" => Auth::user()]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        return view(
            'user.edit',
            [
                "user" => $this->user->findById($id)
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserRequest $request
     * @param User $user
     * @return Illuminate\Http\Redirect
     */
    public function update(UserRequest $request, User $user)
    {
        $dataUpdate['name'] = $request->name;
        $password = $request->password;
        if ( $password !== null & $password !== "" ){
            $password = Hash::make($password);
            $dataUpdate['password'] = $password;
        }
        $this->user->update($user->id, $dataUpdate);
        return redirect()->route('user.index')->withSuccess(
            'User' . $this->user->findById($user->id)->name . " was updated!"
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
    }
}
