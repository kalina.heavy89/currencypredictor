/*
 Navicat Premium Data Transfer

 Source Server         : MyConnection
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : localhost:3306
 Source Schema         : currencyPredictor

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 27/02/2021 16:15:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for access_states
-- ----------------------------
DROP TABLE IF EXISTS `access_states`;
CREATE TABLE `access_states`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `condition` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `period` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of access_states
-- ----------------------------
INSERT INTO `access_states` VALUES (1, 'empty', NULL, NULL, NULL);
INSERT INTO `access_states` VALUES (2, 'expired', NULL, NULL, NULL);
INSERT INTO `access_states` VALUES (3, 'trial', 1, NULL, NULL);
INSERT INTO `access_states` VALUES (4, 'subscripted', 1, NULL, NULL);
INSERT INTO `access_states` VALUES (5, 'subscripted', 3, NULL, NULL);
INSERT INTO `access_states` VALUES (6, 'subscripted', 6, NULL, NULL);
INSERT INTO `access_states` VALUES (7, 'subscripted', 12, NULL, NULL);

-- ----------------------------
-- Table structure for exchange_rate
-- ----------------------------
DROP TABLE IF EXISTS `exchange_rate`;
CREATE TABLE `exchange_rate`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `rate` decimal(10, 2) NOT NULL,
  `rate_date` date NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exchange_rate
-- ----------------------------
INSERT INTO `exchange_rate` VALUES (1, 28.20, '2021-02-23', '2021-02-15 19:43:04', '2021-02-27 06:08:44');
INSERT INTO `exchange_rate` VALUES (2, 28.10, '2021-02-24', '2021-02-15 19:43:04', '2021-02-27 06:08:44');
INSERT INTO `exchange_rate` VALUES (3, 28.20, '2021-02-25', '2021-02-27 05:37:43', '2021-02-27 06:08:44');
INSERT INTO `exchange_rate` VALUES (4, 28.10, '2021-02-26', '2021-02-27 05:37:43', '2021-02-27 06:08:44');
INSERT INTO `exchange_rate` VALUES (5, 28.10, '2021-02-27', '2021-02-27 05:37:43', '2021-02-27 06:08:44');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 61 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (48, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (49, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (50, '2021_01_04_195611_create_users_table', 1);
INSERT INTO `migrations` VALUES (51, '2021_01_04_203125_create_subscription_price_table', 1);
INSERT INTO `migrations` VALUES (52, '2021_01_04_204501_create_exchange_rate_table', 1);
INSERT INTO `migrations` VALUES (53, '2021_01_04_204625_create_predicted_rate_table', 1);
INSERT INTO `migrations` VALUES (54, '2021_01_10_153509_create_access_states_table', 1);
INSERT INTO `migrations` VALUES (55, '2021_01_10_181858_add_soft_delete_to_users_table', 1);
INSERT INTO `migrations` VALUES (56, '2019_05_03_000001_create_customer_columns', 2);
INSERT INTO `migrations` VALUES (57, '2019_05_03_000002_create_subscriptions_table', 2);
INSERT INTO `migrations` VALUES (58, '2019_05_03_000003_create_subscription_items_table', 2);
INSERT INTO `migrations` VALUES (60, '2021_02_06_120118_add_paid_to_subscription_price_table', 3);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for predicted_rate
-- ----------------------------
DROP TABLE IF EXISTS `predicted_rate`;
CREATE TABLE `predicted_rate`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `rate` decimal(10, 2) NOT NULL,
  `rate_date` date NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of predicted_rate
-- ----------------------------
INSERT INTO `predicted_rate` VALUES (1, 28.07, '2021-02-28', '2021-02-27 05:39:38', '2021-02-27 06:08:44');
INSERT INTO `predicted_rate` VALUES (2, 24.98, '2021-03-01', '2021-02-27 05:39:38', '2021-02-27 06:08:44');
INSERT INTO `predicted_rate` VALUES (3, 21.88, '2021-03-02', '2021-02-27 05:39:38', '2021-02-27 06:08:44');

-- ----------------------------
-- Table structure for subscription_items
-- ----------------------------
DROP TABLE IF EXISTS `subscription_items`;
CREATE TABLE `subscription_items`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `subscription_id` bigint(20) UNSIGNED NOT NULL,
  `stripe_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `subscription_items_subscription_id_stripe_plan_unique`(`subscription_id`, `stripe_plan`) USING BTREE,
  INDEX `subscription_items_stripe_id_index`(`stripe_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subscription_items
-- ----------------------------
INSERT INTO `subscription_items` VALUES (1, 1, 'si_Ivokf4n4qdzjP2', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, '2021-02-12 08:35:06', '2021-02-12 08:35:06');
INSERT INTO `subscription_items` VALUES (2, 2, 'si_IvzoJPrhzppx7V', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, '2021-02-12 20:01:18', '2021-02-12 20:01:18');
INSERT INTO `subscription_items` VALUES (3, 3, 'si_Iw0BanrwTwztrT', 'price_1IH07nA88gle3mg5ASQX35pN', 1, '2021-02-12 20:23:24', '2021-02-12 20:23:24');
INSERT INTO `subscription_items` VALUES (4, 4, 'si_IwFoyMbbfkNXtQ', 'price_1IH07nA88gle3mg5ASQX35pN', 1, '2021-02-13 12:33:17', '2021-02-13 12:33:17');
INSERT INTO `subscription_items` VALUES (5, 5, 'si_Iz0y1jaFy3RZcO', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, '2021-02-20 21:24:47', '2021-02-20 21:24:47');
INSERT INTO `subscription_items` VALUES (6, 6, 'si_Iz8pg0bBRRLU2i', 'price_1IH07nA88gle3mg5ASQX35pN', 1, '2021-02-21 05:31:45', '2021-02-21 05:31:45');
INSERT INTO `subscription_items` VALUES (7, 7, 'si_Iz8yxYq0YZkdLv', 'price_1IH07nA88gle3mg5ASQX35pN', 1, '2021-02-21 05:40:28', '2021-02-21 05:40:28');
INSERT INTO `subscription_items` VALUES (8, 8, 'si_Izrg5PsGgkSJk9', 'price_1IH09pA88gle3mg5IP9Ejuhp', 1, '2021-02-23 03:53:03', '2021-02-23 03:53:03');
INSERT INTO `subscription_items` VALUES (9, 9, 'si_IzrsKUYWsOWSYG', 'price_1IH09pA88gle3mg5IP9Ejuhp', 1, '2021-02-23 04:05:15', '2021-02-23 04:05:15');
INSERT INTO `subscription_items` VALUES (10, 10, 'si_Izs0LJraULtF5Q', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, '2021-02-23 04:13:17', '2021-02-23 04:13:17');
INSERT INTO `subscription_items` VALUES (11, 11, 'si_J07qHojzT7cStF', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, '2021-02-23 20:34:50', '2021-02-23 20:34:50');
INSERT INTO `subscription_items` VALUES (12, 12, 'si_J07xvmf6PC1wsT', 'price_1IH07nA88gle3mg5ASQX35pN', 1, '2021-02-23 20:41:26', '2021-02-23 20:41:26');
INSERT INTO `subscription_items` VALUES (13, 13, 'si_J082coIhM8J2Ck', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, '2021-02-23 20:47:08', '2021-02-23 20:47:08');
INSERT INTO `subscription_items` VALUES (14, 14, 'si_J084Xqr8PzBvjp', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, '2021-02-23 20:49:18', '2021-02-23 20:49:18');
INSERT INTO `subscription_items` VALUES (15, 15, 'si_J085yzURWmwBI3', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, '2021-02-23 20:50:14', '2021-02-23 20:50:14');
INSERT INTO `subscription_items` VALUES (16, 16, 'si_J08IkFB90fnlvR', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, '2021-02-23 21:03:08', '2021-02-23 21:03:08');
INSERT INTO `subscription_items` VALUES (17, 17, 'si_J08NzawSRFKjSP', 'price_1IH07nA88gle3mg5ASQX35pN', 1, '2021-02-23 21:07:46', '2021-02-23 21:07:46');
INSERT INTO `subscription_items` VALUES (18, 18, 'si_J08PNdZvjyF6yt', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, '2021-02-23 21:09:52', '2021-02-23 21:09:52');
INSERT INTO `subscription_items` VALUES (19, 19, 'si_J08QcidBtsHvj1', 'price_1IH09pA88gle3mg5IP9Ejuhp', 1, '2021-02-23 21:11:10', '2021-02-23 21:11:10');
INSERT INTO `subscription_items` VALUES (20, 20, 'si_J08SMzfhh1eZ7f', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, '2021-02-23 21:13:15', '2021-02-23 21:13:15');
INSERT INTO `subscription_items` VALUES (21, 21, 'si_J08TXXk3WKYHCY', 'price_1IH09pA88gle3mg5IP9Ejuhp', 1, '2021-02-23 21:14:15', '2021-02-23 21:14:15');
INSERT INTO `subscription_items` VALUES (22, 22, 'si_J08jPfVFsIBUw7', 'price_1IH07nA88gle3mg5ASQX35pN', 1, '2021-02-23 21:29:39', '2021-02-23 21:29:39');
INSERT INTO `subscription_items` VALUES (23, 23, 'si_J1W6vbrQ3XmTb4', 'price_1IH07nA88gle3mg5ASQX35pN', 1, '2021-02-27 13:42:57', '2021-02-27 13:42:57');

-- ----------------------------
-- Table structure for subscription_price
-- ----------------------------
DROP TABLE IF EXISTS `subscription_price`;
CREATE TABLE `subscription_price`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `months` int(11) NOT NULL,
  `price` decimal(10, 2) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `stripe_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subscription_price
-- ----------------------------
INSERT INTO `subscription_price` VALUES (1, 1, 37.00, NULL, NULL, 'price_1IH06IA88gle3mg5z87DIDbZ');
INSERT INTO `subscription_price` VALUES (2, 3, 105.00, NULL, NULL, 'price_1IH07nA88gle3mg5ASQX35pN');
INSERT INTO `subscription_price` VALUES (3, 6, 200.00, NULL, NULL, 'price_1IH08xA88gle3mg5mPjLnK5o');
INSERT INTO `subscription_price` VALUES (4, 12, 377.00, NULL, NULL, 'price_1IH09pA88gle3mg5IP9Ejuhp');

-- ----------------------------
-- Table structure for subscriptions
-- ----------------------------
DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE `subscriptions`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `quantity` int(11) NULL DEFAULT NULL,
  `trial_ends_at` timestamp(0) NULL DEFAULT NULL,
  `ends_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `subscriptions_user_id_stripe_status_index`(`user_id`, `stripe_status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subscriptions
-- ----------------------------
INSERT INTO `subscriptions` VALUES (1, 2, 'cashier', 'sub_IvokQsfmbez5JE', 'active', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, NULL, NULL, '2021-02-12 08:35:06', '2021-02-12 08:35:06');
INSERT INTO `subscriptions` VALUES (2, 2, 'cashier', 'sub_Ivzo0yPCekv89D', 'active', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, NULL, NULL, '2021-02-12 20:01:18', '2021-02-12 20:01:18');
INSERT INTO `subscriptions` VALUES (3, 2, 'cashier', 'sub_Iw0BsYA7xhH7vr', 'active', 'price_1IH07nA88gle3mg5ASQX35pN', 1, NULL, NULL, '2021-02-12 20:23:24', '2021-02-12 20:23:24');
INSERT INTO `subscriptions` VALUES (4, 21, 'cashier', 'sub_IwFozZHPcYSouA', 'active', 'price_1IH07nA88gle3mg5ASQX35pN', 1, NULL, NULL, '2021-02-13 12:33:17', '2021-02-13 12:33:17');
INSERT INTO `subscriptions` VALUES (5, 5, 'cashier', 'sub_Iz0yc0NAM8zSaf', 'active', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, NULL, NULL, '2021-02-20 21:24:47', '2021-02-20 21:24:47');
INSERT INTO `subscriptions` VALUES (6, 9, 'cashier', 'sub_Iz8pwF1BPsW7G6', 'active', 'price_1IH07nA88gle3mg5ASQX35pN', 1, NULL, NULL, '2021-02-21 05:31:45', '2021-02-21 05:31:45');
INSERT INTO `subscriptions` VALUES (7, 9, 'cashier', 'sub_Iz8yEMvLDLaPDo', 'active', 'price_1IH07nA88gle3mg5ASQX35pN', 1, NULL, NULL, '2021-02-21 05:40:28', '2021-02-21 05:40:28');
INSERT INTO `subscriptions` VALUES (8, 11, 'cashier', 'sub_IzrguSz7LPLu9o', 'active', 'price_1IH09pA88gle3mg5IP9Ejuhp', 1, NULL, NULL, '2021-02-23 03:53:03', '2021-02-23 03:53:03');
INSERT INTO `subscriptions` VALUES (9, 11, 'cashier', 'sub_IzrsQj5OomVo1P', 'active', 'price_1IH09pA88gle3mg5IP9Ejuhp', 1, NULL, NULL, '2021-02-23 04:05:15', '2021-02-23 04:05:15');
INSERT INTO `subscriptions` VALUES (10, 11, 'cashier', 'sub_Izs0r5K1bOKHPn', 'active', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, NULL, NULL, '2021-02-23 04:13:17', '2021-02-23 04:13:17');
INSERT INTO `subscriptions` VALUES (11, 9, 'cashier', 'sub_J07q9Q29PX22cC', 'active', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, NULL, NULL, '2021-02-23 20:34:50', '2021-02-23 20:34:50');
INSERT INTO `subscriptions` VALUES (12, 9, 'cashier', 'sub_J07xCajzTII6Nm', 'active', 'price_1IH07nA88gle3mg5ASQX35pN', 1, NULL, NULL, '2021-02-23 20:41:26', '2021-02-23 20:41:26');
INSERT INTO `subscriptions` VALUES (13, 9, 'cashier', 'sub_J082VwERaVZk4X', 'active', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, NULL, NULL, '2021-02-23 20:47:08', '2021-02-23 20:47:08');
INSERT INTO `subscriptions` VALUES (14, 9, 'cashier', 'sub_J084rosBbc0MHg', 'active', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, NULL, NULL, '2021-02-23 20:49:18', '2021-02-23 20:49:18');
INSERT INTO `subscriptions` VALUES (15, 9, 'cashier', 'sub_J085r4naihggcc', 'active', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, NULL, NULL, '2021-02-23 20:50:14', '2021-02-23 20:50:14');
INSERT INTO `subscriptions` VALUES (16, 9, 'cashier', 'sub_J08IZKehMthc7N', 'active', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, NULL, NULL, '2021-02-23 21:03:08', '2021-02-23 21:03:08');
INSERT INTO `subscriptions` VALUES (17, 9, 'cashier', 'sub_J08NB2eN0NGX2L', 'active', 'price_1IH07nA88gle3mg5ASQX35pN', 1, NULL, NULL, '2021-02-23 21:07:46', '2021-02-23 21:07:46');
INSERT INTO `subscriptions` VALUES (18, 9, 'cashier', 'sub_J08Pxh85PaM4Wn', 'active', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, NULL, NULL, '2021-02-23 21:09:52', '2021-02-23 21:09:52');
INSERT INTO `subscriptions` VALUES (19, 9, 'cashier', 'sub_J08Qyrq1MOp1hR', 'active', 'price_1IH09pA88gle3mg5IP9Ejuhp', 1, NULL, NULL, '2021-02-23 21:11:10', '2021-02-23 21:11:10');
INSERT INTO `subscriptions` VALUES (20, 9, 'cashier', 'sub_J08SGxbAmSYfZr', 'active', 'price_1IH06IA88gle3mg5z87DIDbZ', 1, NULL, NULL, '2021-02-23 21:13:15', '2021-02-23 21:13:15');
INSERT INTO `subscriptions` VALUES (21, 9, 'cashier', 'sub_J08TTvh4MeXntN', 'active', 'price_1IH09pA88gle3mg5IP9Ejuhp', 1, NULL, NULL, '2021-02-23 21:14:15', '2021-02-23 21:14:15');
INSERT INTO `subscriptions` VALUES (22, 9, 'cashier', 'sub_J08jKmkBLqnfAY', 'active', 'price_1IH07nA88gle3mg5ASQX35pN', 1, NULL, NULL, '2021-02-23 21:29:39', '2021-02-23 21:29:39');
INSERT INTO `subscriptions` VALUES (23, 2, 'cashier', 'sub_J1W67E3fgEdGSf', 'active', 'price_1IH07nA88gle3mg5ASQX35pN', 1, NULL, NULL, '2021-02-27 13:42:57', '2021-02-27 13:42:57');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `access_cond` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'empty',
  `beg_acc_period` date NULL DEFAULT NULL,
  `end_acc_period` date NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `stripe_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `card_brand` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `card_last_four` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `trial_ends_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE,
  INDEX `users_stripe_id_index`(`stripe_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Deshawn Kovacek', 'anabelle.nicolas@example.org', '2021-01-22 03:22:40', '$2y$10$Vo6zDPi2CIjAFFPckogyyOfqvJMUuNeieRxUQq9nXA3YalSSwDmDO', 'Dl2I1kTuTelQEc6VJqFj0qHVjNQO5QZ9ystKnaUucKNsOO7mmtC9GrDFUx0q', 'trial', '2021-02-20', '2021-03-20', '2021-01-22 03:22:42', '2021-01-22 03:22:42', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (2, 'Rene Wolff', 'nels05@example.org', '2021-01-22 03:22:40', '$2y$10$BDgZg8pyu3KMOnvKaUQgPO6AzPg9wIoA1uLCPbR/hg7rQsqrgs1wS', 'nqTkHRFdO9DRsVDcU35jcsas5ynrOMA527rTivV0qvxMrJ6upDAAJWxfJm52', 'subscripted', '2021-02-12', '2021-08-12', '2021-01-22 03:22:42', '2021-02-27 13:42:57', NULL, 'cus_Ivk8ZPJ5j1HumK', 'mastercard', '4444', NULL);
INSERT INTO `users` VALUES (3, 'Jacynthe Farrell', 'hayden.damore@example.org', '2021-01-22 03:22:40', '$2y$10$/MIEw9sg3d25l4e5aGYrAOF.MxIX2gl2N02.pxBAiqVEjK63x7OMK', 'kCiSRSgtBvjRQGw1M5gs13HdQ2botnB6JoFBmGpAkyjEG8MF7HNGD5BaLxn7', 'subscripted', '2021-01-04', '2021-07-04', '2021-01-22 03:22:42', '2021-01-22 03:22:42', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (4, 'Dr. Dejuan Klein', 'leonie33@example.net', '2021-01-22 03:22:40', '$2y$10$cvQUJYuYqNKySdkNLlBFcub1HpZZce0YWcTeNeIDb6RQoKjTOZEDq', 's9VtcmJlIa5YvX2EKzPn2VutUm24M7MNM28n5p5g6GV03sCeSJgLpTdl5X9I', 'subscripted', '2020-06-26', '2021-06-26', '2021-01-22 03:22:42', '2021-01-22 03:22:42', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (5, 'Jayne Hansen', 'plemke@example.net', '2021-01-22 03:22:40', '$2y$10$V6Bse4KPKw/I.V5VH8KOde3bbFVzPLvikgMNQcFI8RgDAnpcgODKG', 'LObWVp8c5B0dA1kSpBkMqRU9ZS7NP8qGx7wSbzy7xOdc0M3ZE8CF5Zg7ZTdm', 'subscripted', '2021-02-20', '2021-03-20', '2021-01-22 03:22:42', '2021-02-20 21:24:47', NULL, 'cus_Iz0yBGzzBzZQL4', 'visa', '4242', NULL);
INSERT INTO `users` VALUES (6, 'Miss Kali Barrows DVM', 'yazmin46@example.net', '2021-01-22 03:22:41', '$2y$10$Q/2TpYSVdDq7oHXMABuVgOQ1JoUKSelVoojmhhcPCU6/7l62TEmKy', 'TIr733kbONSvN4BJtl2fj9zVo5OBibBflOTZq8yUtxoDKcz1vOv4p1zL0Jxe', 'expired', '2020-12-01', '2021-01-01', '2021-01-22 03:22:42', '2021-01-22 03:22:42', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (7, 'Amely Johns', 'vebert@example.org', '2021-01-22 03:22:41', '$2y$10$WBA2cSrKdH6XEZqEqHf0MOIzsJUNLN158Wcu//hPS1NBdk4DoSage', 'AtM6b0A9Hb', 'empty', NULL, NULL, '2021-01-22 03:22:42', '2021-01-22 03:22:42', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (8, 'Margie Wilderman', 'ryan.daphne@example.org', '2021-01-22 03:22:41', '$2y$10$WfFcGW/K5UGHwcV5CgwLiuJEhAIJEkjPnM/rJc5voBhSaCxoLxDH2', 'miX4Fp27oB', 'empty', NULL, NULL, '2021-01-22 03:22:42', '2021-01-22 03:22:42', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (9, 'Camilla DuBuque', 'verla20@example.net', '2021-01-22 03:22:41', '$2y$10$hmAvXWYFfkcYRiqd5xZyNeKkXHgHZZx7MFVUxy4Z0rNOEsnuPKro.', 'iJSq8prI8qqV484KF6kMm0kxX2690YBezZPQwVPui9f2ySTT503AKAHQJR3S', 'subscripted', '2021-02-23', '2022-06-23', '2021-01-22 03:22:42', '2021-02-23 21:29:39', NULL, 'cus_Iz8pGaTA6uqD4Q', 'visa', '4242', NULL);
INSERT INTO `users` VALUES (10, 'Flavie Boyle', 'emerson.bartoletti@example.com', '2021-01-22 03:22:41', '$2y$10$4e4hnjHI9ihm0TbcNubQqOXGXaN64Z5oDuVT8XQVdzkfZ/yz4AQmS', 'weogoxnOmV', 'empty', NULL, NULL, '2021-01-22 03:22:42', '2021-01-22 03:22:42', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (11, 'Dulce Weimann V', 'gerlach.brayan@example.com', '2021-01-22 03:22:41', '$2y$10$ZQEJGV.xoe8gnBVuPt/zZuWV5RvfnxUyXWqgmKLmbu/1Qu8VKK5vi', 'Vjnbyo6DxGJI1LSZV0f2D5wF04ID75qheAf2F0L20mLPIewRrfg7eDCBIvi9', 'trial', '2021-02-23', '2021-03-23', '2021-01-22 03:22:42', '2021-02-23 03:53:00', NULL, 'cus_IzrgM75de9qGxK', 'visa', '4242', NULL);
INSERT INTO `users` VALUES (12, 'Else Jacobson PhD', 'hirthe.enid@example.net', '2021-01-22 03:22:41', '$2y$10$uilbkrDn/L9aSUyZctuXNOHA8N1jBZmty3Vxyx9e4C6BZsX6BZNnO', 'Gv3cF5KFGo', 'expired', '2020-08-22', '2020-09-22', '2021-01-22 03:22:42', '2021-01-22 03:22:42', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (13, 'Cornelius Kunde PhD', 'koelpin.ila@example.org', '2021-01-22 03:22:41', '$2y$10$ScWEBleIJrGJDAMnv0ed1.d8DcqdMAmRsBGo3fMKOANWVRbjBaVYa', 'cbbnehsdO8', 'expired', '2020-07-24', '2020-08-24', '2021-01-22 03:22:42', '2021-01-22 03:22:42', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (15, 'Morgan Kihn', 'estella.mclaughlin@example.org', '2021-01-22 03:22:41', '$2y$10$TKk84mFTlCFfQiSLPhDb2erECvSxXb6IndONTlBD9GlWsRm06m.fW', 'te4EVA1yfoTob7N5vWb5cjYpRLca1dPLw5yjZgRhS7P7wx99OIc57m8UZN5m', 'subscripted', '2020-03-05', '2021-03-05', '2021-01-22 03:22:42', '2021-01-22 03:22:42', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (16, 'Prof. Eric Eichmann DDS', 'timothy57@example.com', '2021-01-22 03:22:41', '$2y$10$8LXR2eYtUboVS/L9WFhiyOvF9RT5Cm6v49tSFD9fOdNMo7a.R5XRe', 'nLkkpEoAky', 'subscripted', '2021-01-12', '2021-07-12', '2021-01-22 03:22:42', '2021-01-22 03:22:42', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (17, 'Mrs. Destiney Kuphal', 'bednar.lesley@example.com', '2021-01-22 03:22:41', '$2y$10$nh4ZVAcgENPcqgl2GYCU7u0mWIqd2Kp5VlDOZgn0hHBAuCvba1rt2', 'Mt8kiHdi3C', 'expired', '2020-06-25', '2020-07-25', '2021-01-22 03:22:42', '2021-01-22 03:22:42', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (18, 'Prof. Amie Cormier', 'wwhite@example.net', '2021-01-22 03:22:42', '$2y$10$g1Jak7/9YjK2dJpeYZsPqemSTRuBA6cXFhzVU7.NCvEvd8bnlsUfm', 'rm5Zv0AD7h', 'empty', NULL, NULL, '2021-01-22 03:22:42', '2021-01-22 03:22:42', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (19, 'Abigayle Oberbrunner', 'wharber@example.org', '2021-01-22 03:22:42', '$2y$10$nP0hnszuVDx5c8xuhmGJu.jwT.7aqK5CXtkcBZiCCxuO1EJfl7ZNK', 'TEmn3yh08t', 'subscripted', '2021-01-21', '2021-04-21', '2021-01-22 03:22:42', '2021-01-22 03:22:42', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (20, 'Dr. Aaron Howe', 'xfeeney@example.com', '2021-01-22 03:22:42', '$2y$10$32pvvJUcBakwlXf5y7T3J.flotpERXjwQzD5awp6CUNE9vCSurXo6', 'zGkM0xKTTZ', 'expired', '2020-01-06', '2020-02-06', '2021-01-22 03:22:42', '2021-01-22 03:22:42', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (21, 'user1', 'user1@mail.com', '2021-02-13 12:31:00', '$2y$10$TUC8lei/i7nJH6Qaauedt.o2.aYaX6bA4GYF56KjS88Mk3xJYit8e', NULL, 'subscripted', '2021-02-13', '2021-05-13', '2021-02-13 12:29:00', '2021-02-13 12:33:17', NULL, 'cus_IwFoAUjRNl1qck', 'visa', '4242', NULL);

SET FOREIGN_KEY_CHECKS = 1;
