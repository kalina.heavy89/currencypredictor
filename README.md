# ![Laravel Example App](logo.png)

[![Build Status](https://img.shields.io/travis/gothinkster/laravel-realworld-example-app/master.svg)](https://travis-ci.org/gothinkster/laravel-realworld-example-app) [![Gitter](https://img.shields.io/gitter/room/realworld-dev/laravel.svg)](https://gitter.im/realworld-dev/laravel) [![GitHub stars](https://img.shields.io/github/stars/gothinkster/laravel-realworld-example-app.svg)](https://github.com/gothinkster/laravel-realworld-example-app/stargazers) [![GitHub license](https://img.shields.io/github/license/gothinkster/laravel-realworld-example-app.svg)](https://raw.githubusercontent.com/gothinkster/laravel-realworld-example-app/master/LICENSE)

> ### Example Laravel codebase containing real world examples (CRUD, auth, advanced patterns and more) that adheres to the [RealWorld](https://github.com/gothinkster/realworld-example-apps) spec and API.

This repo is functionality complete — PRs and issues welcome!

----------

# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/8.0/installation#installation)

Clone the repository

    git@gitlab.com:kalina.heavy89/currencypredictor.git

Switch to the repo folder

    cd /currencyPredictor.loc

Install all the dependencies using composer

    composer install

Download NVM from site https://github.com/nvm-sh/nvm using the cURL or Wget command:

    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash

    wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash

Add the source line to the correct profile file

    source ~/.profile

Download, compile, and install the latest release of node

    nvm install node

Install the packages Node.js

    npm install

Run all tasks MIX

    npm run dev

Copy the .env.example file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key and write it to the .env file

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Run the database seeder

    php artisan db:seed

Run the scheduler to get currency exchange rate from Privat Bank API

    php artisan schedule:work



You can now access the server at http://currencyPredictor.loc

**TL;DR command list**

    git@gitlab.com:kalina.heavy89/currencypredictor.git
    cd currencyPredictor.loc
    composer install
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash     or
    wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
    source ~/.profile
    nvm install node
    npm install
    npm run dev
    cp .env.example .env
    php artisan key:generate
    php artisan migrate
    php artisan db:seed
    php artisan schedule:work

**Make sure you set the correct database connection information before running the migrations** [Environment variables](#environment-variables)

    php artisan migrate

***Note*** : It's recommended to have a clean database before seeding. You can refresh your migrations at any point to clean the database by running the following command

    php artisan migrate:refresh  
