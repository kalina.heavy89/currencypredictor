<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\UserRepository;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Auth;

class UserService extends BaseService
{
    public function __construct(UserRepository $repo)
    {
        $this->repo = $repo;
    }

    public function checkUserAccCond ($userId) {
        $user = $this->repo->findById($userId);
        if (($user->access_cond == 'trial') || ($user->access_cond == 'subscripted')) {
          //If access_cond is trial or subscripted checks the end of accessing period
            $currentDateCompare = Helper::dateAnalizing($user->end_acc_period);
            if ($currentDateCompare < 0) {
                $user->access_cond = 'expired';
                //Changing data in table users:
                $user->save();
                //$user = $this->repo->update($id, $user);
            }
        }
        return $user->fresh();
    }

    public function createSetupIntent()
    {
        $user = Auth::user();
        return $user->createSetupIntent();
    }

}
