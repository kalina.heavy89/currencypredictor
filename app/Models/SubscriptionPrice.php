<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubscriptionPrice extends Model
{
    use HasFactory;

    protected $table = 'subscription_price';

    public $incrementing = false;

    protected $fillable = [
        'id',
        'months',
        'price',
        'stripe_id'
    ];

}
