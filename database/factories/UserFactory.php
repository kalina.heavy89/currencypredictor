<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;
	
	private function getAccCond ()
	{
		$accessType = ['empty', 'trial', 'subscripted'];
		$r = rand(0, 2);
		return $accessType[$r];
	}
	
	private function getAccConditionWithPeriod ()
    {
        $period = [1, 3, 6, 12];
		$condition = $this->getAccCond();
		
		if (($condition == 'trial') || ($condition == 'subscripted')) {//if we have trial or subscripted condition
			//defining $begAccPeriod
			$year = mt_rand(date("Y")-1, (date("Y")));
			if ($year < (date("Y"))) {
				$month = mt_rand(1, 12);
				$day = mt_rand(1, 28);
			} else {						
				$month = mt_rand(1, date("m"));
				if ($month < date("m")) {
					$day = mt_rand(1, 28);
				} else {
					$day = mt_rand(1, date("d"));
				}
			}
			$begAccPeriod = $year . "-" . $month . "-" . $day;
			
			//finding $endAccPeriod	
			if ($condition == 'trial') {
				$month += 1;
			} else {
				$randPeriod = $period[mt_rand(0, (count($period)-1))];
				$month += $randPeriod;
			}
			if($month > 12) {
				$year++;
				$month -= 12;
			}
			$endAccPeriod = $year . "-" . $month . "-" . $day;
			
			//analizing $endAccPeriod relatively current date
			$currentDateCompare = Helper::dateAnalizing($endAccPeriod);
			if ($currentDateCompare < 0) {
				$condition = 'expired';
			}			
			return [$condition, $begAccPeriod, $endAccPeriod];
		} else {
			return [$condition, null, null];
		}
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user = Auth::user();
        event(new Registered($user));
		$accessConditions = $this->getAccConditionWithPeriod ();
		return [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => Hash::make('11111111'), // password
            'remember_token' => Str::random(10),
			'access_cond' => $accessConditions[0],
			'beg_acc_period' => $accessConditions[1],
			'end_acc_period' => $accessConditions[2]
        ];
    }
}
