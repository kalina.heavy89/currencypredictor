<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\PredictedRate;

class PredictedRateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		for ($i = 0; $i <3; $i++) {
            $date = date('Y-m-d');
            DB::table('predicted_rate')->insert([
                'id' => $i+1,
                'rate' => mt_rand(2650, 2950)/100,
                'rate_date' => date('Y-m-d', strtotime($date . $i . " days"))
            ]);
        }
    }
}
