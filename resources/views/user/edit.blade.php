@extends('adminlte::page')

@section('content_header')
    <div class="starter-template text-center py-2 px-3">
        <h1>Edit Profile</h1>
    </div>
@stop

@section('content')
    <div class="md-8">
        
                <form method="POST" action="{{ route('user.update', ['user' => $user->id]) }}">
                    @method('PUT')
                    @csrf
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            
                                <div class="card-body">
                                    <div class="form-group row">                                        
                                        <label class="col-md-4 col-form-label" for="name">New Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{old('name') ?? $user->name }}">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="password">New Password</label>  
                                        <div class="col-md-8">                              
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password">
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror 
                                        </div>                               
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="password-confirm">Confirm Password</label>
                                        <div class="col-md-8 pull-right">
                                            <input id="password-confirm" type="password" class="form-control " name="password_confirmation">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            
                        </div>
                        <div class="card-footer box-profile" style="text-align: center;">
                            <input type="submit" value="Save Changes" class="btn btn-primary" style="margin-top: 20px;">
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
@stop