@extends('layouts.template')

@section('header')

<nav class="navbar_landing">
  <div class="container-fluid">
    <div>
      <a class="navbar-brand" href="{{ url('/') }}">{{ __('Go Back') }}</a>
    </div>       
  </div>
</nav>
<div class="title_header py-2 px-3">
    <h1>Currency Predictor</h1>
</div>

@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
