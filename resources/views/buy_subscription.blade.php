@extends('adminlte::page')

@section('content_header')
    <div class="starter-template text-center py-2 px-3">
        <h1>Buy Subscription</h1>
    </div>
@stop

@section('content')

<div class="container">

    <form action="{{ route('subscribe.post') }}" method="post" id="payment-form" data-secret="{{ $intent->client_secret }}">
        @csrf
        
        <div>

            <label for="cardholder-name">Cardholder's Name</label>
            <div>
                <input type="text" id="cardholder-name" class="w-1/2 px-2 py-2 border" value="{{ Auth::user()->name }}">
            </div>

            <?php $i = 0; ?>
            @foreach ($prices as $price)
                
                <input type="radio" id="{{ $price->id }}" name="price" value="{{ $price->stripe_id }}"
                @if ($i == 0)
                    checked="checked"
                @endif
                >
                <label for="{{ $price->id }}">
                    {{ $price->months }}
                    @if ($i > 0)
                        monthes 
                    @else
                        month
                    @endif
                    {{ $price->price }}
                     UAH                        
                </label><br>
                <?php $i++; ?>
            @endforeach
        </div>

        <div>
            <label for="card-element">
                Credit or debit card
            </label>
            <div class="col-6">
              <p class="lead">Payment Methods:</p>
              <img src="/images/visa.png" alt="Visa">
              <img src="/images/mastercard.png" alt="Mastercard">
              <img src="/images/american-express.png" alt="American Express">
            </div>
            <br>
            <div id="card-element" style="width: 75%;">
                <!-- A Stripe Element will be inserted here. -->
            </div>

            <!-- Used to display Element errors. -->
        <div id="card-errors" role="alert"></div>
        </div>
        
        <button type="submit" class="btn btn-success" style="margin-top: 20px;">
            <b>Subscribe Now</b>
        </button>            

    </form>

</div>

@stop

@section('js')
    <script src="https://js.stripe.com/v3/"></script>

    <script>
        // Set your publishable key: remember to change this to your live publishable key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        var stripe = Stripe(
            'pk_test_51IDrwCA88gle3mg5Yf0WYFznwgtl03dfmae5jBTZDg4sDEYC8WqNeaVQ9dSMZ5F4pBZTZcJ1eduYpPdqS8gC2icW00UW2nn4X8'
        );
        var elements = stripe.elements();

        // Custom styling can be passed to options when creating an Element.
        var style = {
          base: {
            // Add your base input styles here. For example:
            fontSize: '16px',
            color: '#32325d',
          },
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {style: style});            

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');

        // Create a token or display an error when the form is submitted.
        var form = document.getElementById('payment-form');
        var cardHolderName = document.getElementById('cardholder-name');
        var clientSecret = form.dataset.secret;

        form.addEventListener('submit', async function(event) {
          event.preventDefault();

          const { setupIntent, error } = await stripe.confirmCardSetup(
          clientSecret, {
              payment_method: {
                  card,
                  billing_details: { name: cardHolderName.value }
              }
          });

          if (error) {
              //Inform the customer that there was an error.
              var errorElement = document.getElementById('card-errors');
              errorElement.textContent = error.message;
          } else {
              // Send the token to your server.
              //console.log(setupIntent);
              stripeTokenHandler(setupIntent);
          }

        });

        function stripeTokenHandler(setupIntent) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('payment-form');
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'paymentMethod');
            hiddenInput.setAttribute('value', setupIntent.payment_method);
            form.appendChild(hiddenInput);

            // Submit the form
            form.submit();
        }
    </script>
@stop