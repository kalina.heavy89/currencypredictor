<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use App\Repositories\BaseRepository;

abstract class BaseService
{
    public $repo;

    /**
    * Constructor
    *
    * @var BaseRepository $repo
    */
    public function __construct(BaseRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
    * Get all data
    *
    * @return Collection
    */
    public function all(): Collection
    {
        return $this->repo->all();
    }

    /**
    * Create new record
    *
    * @param array $input
    * @return model
    */
    public function create(array $data): Model
    {
        return $this->repo->create($data);
    }

    /**
    * Find record by id
    *
    * @param int $id
    * @return Model
    */
    public function findById(int $id): Model
    {
        return $this->repo->findById($id);
    }

	/**
    * Find record by fieldValue in fieldName
    *
    * @param int $fieldName, string $fieldValue
    * @return Model
    */
    public function findByField(string $fieldName, string $fieldValue): Model
    {
        return $this->repo->findByField($fieldName, $fieldValue);
    }

    /**
    * Get the column by field name.
    *
    * @param string $fieldName
    * @return model
    */
    public function getByField(string $fieldName): Model
    {
        return $this->repo->getByField($fieldName);
    }

    /**
    * Show the first record from the database
    *
    * @return model
    */
    public function first()
    {
        return $this->repo->first();
    }

    public function last()
    {
        return $this->repo->last();
    }

    /**
    * Update data
    *
    * @param integer $id
    * @param array $data
    * @return boolean
    */
    public function update(string $id, array $data)
    {
        return $this->repo->update($id, $data);
    }

    /**
    * Delete record by id
    *
    * @param integer $id
    * @return boolean
    */
    public function destroy(string $id): bool
    {
        return $this->repo->destroy($id);
    }

}
