@extends('adminlte::page')

@section('content_header')
    <div class="starter-template text-center py-2 px-3">
        <h1>Exchange Rate</h1>
    </div>
@stop

@section('content')
    <div class="row">
    <div class="md-2"></div>
    <div class="md-8">
    <canvas id="canvas"></canvas>
    </div>
    <div class="md-2"></div>
    </div>

    <div style="text-align: center;">
        <a href="{{ route('show') }}" class="btn btn-primary" style="margin-top: 20px;"><b>Update Graph</b></a>
    </div>
@stop

@section('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.0.2/chart.min.js"></script>

    <script>

        var ctx = document.getElementById('canvas');

        /*
        backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
        ];
        borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
        ];
        */

        const date = <?php echo $date; ?>;
        const existingRate = <?php echo $existingRate; ?>;
        const predictedRate = <?php echo $predictedRate; ?>;

        const data = {
            labels: date,
            datasets: [
                {
                    label: 'Existing Exchange Rate',
                    data: existingRate,
                    borderColor: 'rgba(255, 99, 132, 1)',
                    backgroundColor: 'rgba(255, 99, 132, 1)',
                },
                {
                    label: 'Predicted Exchange Rate',
                    data: predictedRate,
                    borderColor: 'rgba(54, 162, 235, 1)',
                    backgroundColor: 'rgba(54, 162, 235, 1)',
                }
            ]
        };

        const config = {
            type: 'line',
            data: data,
            options: {
                responsive: true,
                plugins: {
                legend: {
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'USD-UAH Exchange Rate'
                }
                }
            },
        };

        var chart = new Chart(ctx, config);

        chart.update();

    </script>

@stop
