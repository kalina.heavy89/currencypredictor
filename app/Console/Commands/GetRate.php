<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\ExchangeRateService;
use App\Helpers\Helper;

class GetRate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rate:uah';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get current date usd -> uah rate';

    /**
     * The value of existing rate.
     *
     */
    private $existRate;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ExchangeRateService $existRate)
    {
        parent::__construct();
		$this->existRate = $existRate;
    }

    /**
     * Execute the console command.
     *
     * @return null
     */
    public function handle()
    {
        $exchangeRate = $this->existRate->last();
        if (
            (Helper::dateAnalizing($exchangeRate->rate_date) < 0) &&
            (date("H", strtotime("+2 hours")) < 13)
        ) {
            $this->existRate->saveDataRate();
        }
        return 0;
    }
}
