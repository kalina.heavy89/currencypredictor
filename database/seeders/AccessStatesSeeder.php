<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\AccessState;

class AccessStatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $conditions = ['empty', 'expired', 'trial', 'subscripted', 'subscripted', 'subscripted', 'subscripted'];
		$periods = [null, null, 1, 1, 3, 6, 12];
		for ($i = 0; $i < count($periods); $i++) {
            DB::table('access_states')->insert([
                'id' => $i+1,
                'condition' => $conditions[$i],
                'period' => $periods[$i]
            ]);
        }
    }
}
