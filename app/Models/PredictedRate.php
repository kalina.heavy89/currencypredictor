<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PredictedRate extends Model
{
    use HasFactory;

    protected $table = 'predicted_rate';

    public $incrementing = false;

    protected $fillable = [
        'id',
        'rate',
        'rate_date'
    ];
}
