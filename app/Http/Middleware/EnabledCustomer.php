<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class EnabledCustomer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->user() && !(($request->user()->access_cond == 'subscripted') || ($request->user()->access_cond == 'trial'))) {
            return redirect('home');
        }
        return $next($request);
    }
}
