<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\UserService;
use App\Services\SubscriptionService;

class SubscriptionBuyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @param UserService $user
     * @param SubscriptionService $subscription
     * @return void
     */
    public function __construct(
        UserService $user,
        SubscriptionService $subscription
    ) {
        $this->middleware(['auth']);
        $this->user = $user;
        $this->subscription = $subscription;
    }

    /**
     * Set trial period
     *
     * @return Illuminate\Http\Redirect
     */
    public function setTrialPeriod()
    {
        $userId = Auth::id();
        $subscription = $this->subscription->setTrialPeriod($userId);
        return redirect()->route('home');
    }

    /**
     * Get subscription prices
     *
     * @return \Illuminate\View\View
     */
    public function getSubscriptionPrices()
    {
        $prices = $this->subscription->getSubscriptionPrices();
        $intent = $this->user->createSetupIntent();

        return view('buy_subscription')->with(['prices' => $prices, 'intent' => $intent]);
    }

    /**
     * Buy subscription
     *
     *  @param Request $request
     *  @return Illuminate\Http\Redirect
     */
    public function buySubscription(Request $request)
    {
        $this->subscription->buySubscription($request);
        return redirect('home')
            ->withSuccess('The Payment Was Successful!');
    }

}
