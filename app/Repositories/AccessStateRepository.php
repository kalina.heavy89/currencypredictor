<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\AccessState;

class AccessStateRepository extends BaseRepository
{
    /**
     * Constructor
     *
     * @var AccessState $model
     */
    public function __construct(AccessState $model)
    {
        $this->model = $model;
    }

}
