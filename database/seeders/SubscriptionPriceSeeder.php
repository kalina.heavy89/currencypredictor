<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\SubscriptionPrice;

class SubscriptionPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $months = [1, 3, 6, 12];
		$priceMonth = 37;
		for ($i=0; $i<4; $i++) {
			DB::table('subscription_price')->insert([
                'id' => $i+1,
				'months' => $months[$i],
				'price' => round($priceMonth*$months[$i]*(1-0.05*$i))
			]);
		}
    }
}
